# PoC - Web component using TailwindCSS

This resource contains two projects to test and validate the usability of TailwindCSS as styles and layouts framework.

## Available Scripts

In the project directory there is one project called tailwind-css-po that contains the ReactJs project with the Race Bets Component that will be used in an IFrame.
I have deployed it in Heroku and you can see it running there [race bets component](https://race-bets-test.herokuapp.com/)

The second project is the iframe-test, that is also a ReactJs application that runs the first as an IFrame.
I am also using Heroku to run it and you can take a look at [race bets component as Iframe](https://iframe-race-bets-test.herokuapp.com/)

## Considerations

All the configurations I had to make in the race bets component for it works were quite straightforward. The TailwindCSS documentation is really clean and very easy to follow.

Another great point for Tailwind is the community is very active, and there are many discussions about it in Github, CSS-tricks, and other places, making it much simpler to troubleshoot whenever you have anything going on.

There are many different ways to set up the framework's settings, which is really awesome, once it allows you to find the better choice for each project you have to develop. For instance, when I was reading its documentation, they give you those choices I mentioned and based on them and some implementations I looked at, I've chosen the best option for what I needed at the moment. You can take a look at some articles I've found as an example of one way to configure it. [Article about dark theme settings](https://jeffjadulco.com/blog/dark-mode-react-tailwind/#extending-tailwind-css), [CSS-Tricks article about theming](https://css-tricks.com/color-theming-with-css-custom-properties-and-tailwind/), [Tailwind section about theming](https://tailwindcss.com/docs/theme), and [Tailwind section about color settings](https://tailwindcss.com/docs/customizing-colors).

Then, continuing at this topic, there are also many ways to set up the purge CSS settings. Thus, just by following the best practices, I've seen some community's discussion and Tailwind's documentation I could easily implement this great feature. [Tailwind section about optimization](https://tailwindcss.com/docs/optimizing-for-production). One good point here is the easy way we have to test it when running the application mentioned before, you can simply change one style by one that was purged, and it won't have any effect.
Example:
![ Alt text](https://bitbucket.org/FelipeMathieu/tailwindcsspo/raw/abeccf7ff70ec2001bdeb564727e3294ce1bb42e/change-css-example.gif)

## Conclusion

Basically, I only see advantages to use TailwindCSS as utility first framework for a project. It is well documented, it has a really active community, which helps a lot the developers, and it has a great and solid way to handle styles and layouts in the application. Along with it, an amazing part of it is, the styles and layouts are totally configurable as much as we need, making it an awesome tool to build what we need according to each project.
The implementation of its settings is very straightforward and it allows you to have a really nice application, not only a pretty design but also as performant as possible.
If I had to say whether to use it or not, I'd vote for yes.
The only concern I had when using it was, I've used Angular flex layout that is a package for Angular that works a little bit similar to what Tailwind does, but more focused on the layout. [Angular flex layout](https://tburleson-layouts-demos.firebaseapp.com/#/docs). It works very well, but it has a bug that is quite tricky when you use print preview and it triggers the resize process [Angular flex layout issue](https://tburleson-layouts-demos.firebaseapp.com/#/docs).
However, I didn't find anything related to it for Tailwind, so that good.
Finally, considering that my concern seems not to be true for TW, and the learning curve tends to be reasonably fast, I'd like to see it working in a real application.
