module.exports = {
  purge: {
    mode: 'all',
    preserveHtmlElements: false,
    content: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html']
  },
  darkMode: 'class',
  theme: {
    backgroundColor: theme => ({
      ...theme('colors'),
      primary: 'var(--color-bg-primary)',
      secondary: 'var(--color-bg-secondary)',
      accent: 'var(--color-bg-accent)',
      'btn-primary': 'var(--btn-primary)',
    }),
    textColor: theme => ({
      ...theme('colors'),
      accent: 'var(--color-text-accent)',
      primary: 'var(--color-text-primary)',
      secondary: 'var(--color-text-secondary)',
    }),
    extend: { },
  },
  variants: {
    extend: {
      typography: ['dark']
    }
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
}
