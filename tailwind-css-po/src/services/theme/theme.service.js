import { ThemeTypes } from "../../utils/theme.types";

export const setTheme = (isActive) => {
    localStorage.theme = isActive ? ThemeTypes.dark : ThemeTypes.light;

    if (localStorage.theme === ThemeTypes.dark || (!('theme' in localStorage) && window.matchMedia(`(prefers-color-scheme: ${ThemeTypes.dark})`).matches)) {
        document.documentElement.classList.add(ThemeTypes.dark);
        document.documentElement.classList.remove(ThemeTypes.light);
    } else {
        document.documentElement.classList.add(ThemeTypes.light);
        document.documentElement.classList.remove(ThemeTypes.dark);
    }
}

export const getTheme = () => {
    const theme = localStorage.theme;
    if(!theme) {
        localStorage.theme = ThemeTypes.light;
    }
    return localStorage.theme;
}