import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { getTheme, setTheme } from '../../../services/theme/theme.service';
import { ThemeTypes } from '../../../utils/theme.types';
import './index.css'

class Header extends React.Component {
    state = {
        isActive: false
    };

    componentDidMount() {
        this.setActiveFlag(getTheme() === ThemeTypes.dark);
    }

    getIcon = () => this.state.isActive ? faSun : faMoon;

    setActiveFlag = (value) => {
        setTheme(value);
        this.setState({
            isActive: value
        });
    }

    render () {
        return (
            <div className="flex justify-end">
                <label htmlFor="toogleA"
                    className="flex items-center cursor-pointer">
                    <div className="relative">
                        <input id="toogleA" type="checkbox" className="hidden"
                            checked={ this.state.isActive }
                            onChange={ () => this.setActiveFlag(!this.state.isActive) }/>
                        <div className="toggle__line w-10 h-4 rounded-full shadow-inner bg-accent" />
                        <div className="toggle__dot flex items-center justify-center absolute w-6 h-6 rounded-full shadow inset-y-0 left-0 bg-btn-primary">
                            <FontAwesomeIcon className="text-secondary"
                                icon={ this.getIcon() } />    
                        </div>
                    </div>
                </label>
            </div>
        );
    }
}

export default Header;