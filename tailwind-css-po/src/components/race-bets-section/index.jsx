import { useEffect, useState } from "react";
import { getNavCurrentState } from "../../core/ui-state/nav-state/nav.query";
import navStore from "../../core/ui-state/nav-state/nav.store";
import Banner from "./banner";
import FAQ from "./faq";
import Header from "./header";
import LoginBody from "./login-body";
import TandC from "./t-and-c";
import Toolbar from "./toolbar";

const RaceBets = () => {
    const [selectedIndex, setIndex] = useState();
    const unsubscribe = navStore.subscribe(() => updateIndex());

    useEffect(() => {
        updateIndex();
        return () => {
            setIndex(0);
            unsubscribe();
        };
    });

    const updateIndex = () => {
        const currentState = getNavCurrentState();
        setIndex(currentState.selectedIndex);
    }

    const getBody = () => {
        switch(selectedIndex) {
            case 1:
                return <FAQ />;
            case 2:
                return <TandC />
            default:
                return <LoginBody />;
        }
    }
    
    return (
        <div className="flex flex-col shadow-lg w-full gap-y-4 p-8 bg-primary dark:bg-secondary">
            <Header />
            <Banner />
            <Toolbar />
            { getBody() }
        </div>
    );
}

export default RaceBets;