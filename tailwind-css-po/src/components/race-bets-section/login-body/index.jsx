import React from 'react';
import './index.css';

const LoginBody = () => {
    return (
        <div className="flex justify-center shadow-inner bg-secondary dark:bg-primary bg-opacity-25">
            <div className="flex flex-col items-center justify-center p-8 gap-y-4 text-center">
                <p className="text-2xl">You're currently Logged Out</p>
                <span>
                    The page you are trying to access is available for registered customers only. Please login to proceed.
                </span>
                <button className="login-button bg-accent hover:bg-btn-primary focus:outline-none focus:ring-opacity-75">
                    Login
                </button>
            </div>
        </div>
    );
}

export default LoginBody;