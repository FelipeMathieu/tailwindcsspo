import React from 'react';

const Banner = () => {
    return (
        <div className="h-48 border-dashed border-4 text-center font-extrabold flex items-center justify-center rounded-md">
            <div>
                BANNER
            </div>
        </div>
    );
}

export default Banner;