import { useState } from "react";
import { updateSelectedIndex } from "../../../../core/ui-state/nav-state/nav.query";
import './index.css';

const TabsSection = () => {
    const [openTab, setOpenTab] = useState(0);
    const [hasChanged, onTabChange] = useState(false);

    const getLinkClasses = () => 'text-xs dark:text-white font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal';

    const getListClasses = () => '-mb-px mr-2 last:mr-0 flex-auto text-center';

    const getBackgroundColor = (index) => openTab === index ? 'bg-accent text-white' : 'bg-primary';

    const setTabIndex = (index) => {
        updateSelectedIndex(index);
        setOpenTab(index);
        onTabChange(false);
    }

    return (
        <div>
          <label htmlFor="menu-toggle" className="pointer-cursor flex items-end justify-end lg:hidden">
            <svg className="fill-current" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
              <title>
                menu
              </title>
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
            </svg>
          </label>
          <input className="hidden" type="checkbox" id="menu-toggle"
            checked={ hasChanged }
            onChange={ () => onTabChange(!hasChanged) } />

          <div className="hidden lg:flex lg:flex-wrap lg:items-center lg:w-auto w-full sm:gap-y-4" id="menu">
            <ul className="flex mb-0 list-none flex-wrap pt-3 pb-4 flex-row"
              role="tablist">
              <li className={ getListClasses() }>
                <a className={ `${getLinkClasses()} ${getBackgroundColor(0)}` }
                  onClick={e => {
                    e.preventDefault();
                    setTabIndex(0);
                  }}
                  data-toggle="tab"
                  href="#game"
                  role="tablist">
                  Game
                </a>
              </li>
              <li className={ getListClasses() }>
                <a className={ `${getLinkClasses()} ${getBackgroundColor(1)}` }
                  onClick={e => {
                    e.preventDefault();
                    setTabIndex(1);
                  }}
                  data-toggle="tab"
                  href="#faq"
                  role="tablist">
                  FAQ
                </a>
              </li>
              <li className={ getListClasses() }>
                <a className={ `${getLinkClasses()} ${getBackgroundColor(2)}` }
                  onClick={e => {
                    e.preventDefault();
                    setTabIndex(2);
                  }}
                  data-toggle="tab"
                  href="#tcs"
                  role="tablist">
                  T&Cs
                </a>
              </li>
            </ul>
          </div>
        </div>
    );
}

export default TabsSection;