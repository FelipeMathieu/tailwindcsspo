import React from 'react';
import TabsSection from './tabs-section';

const Toolbar = () => {
    return (
        <div className="flex items-center justify-between">
            <p className="text-3xl">The RaceBets Million</p>
            <TabsSection />
        </div>
    );
}

export default Toolbar;