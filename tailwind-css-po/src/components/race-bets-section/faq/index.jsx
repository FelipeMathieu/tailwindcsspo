const FAQ = () => {
    const faqData = [
        {
            title: 'Who can take part?',
            description: 'You must have a registered RaceBets account and be eligible for promotions and bonuses to take part'
        },
        {
            title: 'How many questions do I need to answer?',
            description: 'The number of questions you need to answer will be displayed when you play the game, typically this will be 20 questions.'
        },
        {
            title: 'What is a pot?',
            description: 'Usually the “pot” is known as your potential winnings, however for the RaceBets Million we give you your potential winnings up front and it is your job to keep it.'
        },
        {
            title: 'What happens if I do not answer a question in time?',
            description: 'You get sixty days to answer a question before the game ends. If you do not answer in this time,' +
            ' you will be entitled to claim an amount that is calculated by halving the pot (i.e. as it stands at the lapse of the sixty-day period) for every remaining question.' +
            ' For example, if you have £/€100 in the pot with two rounds to play, you will be entitled to claim £/€25 (because the remaining £/€100' +
            ' will be notionally halved and the £/€50 which remains after that will then be notionally halved again). Please note that, because you have allowed the game to lapse,' +
            ' you will not automatically be credited with this amount but will be entitled to claim it by contacting our customer services team.'
        },
        {
            title: 'Do I answer questions one by one?',
            description: 'Yes, once the question you have answered has been resulted you will be able to answer the next question'
        },
        {
            title: 'How much of my pot can I split?',
            description: 'If you go all in, this means that if you pick the correct answer all of your pot will move on to the next question.'+
            ' If you are all in on the wrong answer you will have lost and be out of the game.'
        },
        {
            title: 'How are results settled?',
            description: 'Results are settled in accordance with our sportsbook rules.'
        },
        {
            title: 'Where can I see my previous answers?',
            description: 'Previous answers can be found under “History” on The RaceBets Million page.'
        },
        {
            title: 'How can I enter?',
            description: 'You will be able to enter RaceBets Million when you cumulatively stake £25.' +
            ' You can only play one game at any one time and the £25 staking requirement starts again once a game has finished.'
        },
    ];

    const getDataClasses = () => 'flex flex-col items-start justify-start p-8 gap-y-4 text-start';

    const renderFaqData = () => faqData.map((item, index) => {
        return (
            <div key={ index } className={ getDataClasses() }>
                <p className="text-2xl">{ item.title }</p>
                <span>{ item.description }</span>
            </div>
        )
    });

    return (
        <div className="flex flex-col justify-start shadow-inner bg-secondary dark:bg-primary bg-opacity-25">
            <div className={ getDataClasses() }>
                <p className="text-2xl">How does it work?</p>
                <span>
                    Step 1: Check if you have a ticket to play RaceBets Million, tickets will be issued when you have staked £25
                </span>
                <span>
                    Step 2: You will be presented with your first question to answer, you can either split your pot or go all in on one answer, you then need to lock your answer. Once the question has been resulted, the money you placed on the correct answer will be available to be staked on the next question. You repeat this process until all the questions have been answered.
                </span>
                <span>
                    Step 3: Any money left in the pot after all the questions have been answered is yours to keep.
                </span>
            </div>
            { renderFaqData() }
        </div>
    );
}

export default FAQ;