const TandC = () => {
    const getDataClasses = () => 'flex flex-col items-start justify-start p-8 gap-y-4 text-start';

    return (
        <div className="flex flex-col justify-start shadow-inner bg-secondary dark:bg-primary bg-opacity-25">
            <div className={ getDataClasses() }>
                <p className="text-2xl">Who can take part?</p>
                <span>
                    The promotion is available for new and existing customers with registered RaceBets accounts, aged 18 or over.
                </span>
            </div>
            <div className={ getDataClasses() }>
                <p className="text-2xl">How and when can I qualify?</p>
                <span>
                    <p>Customers will qualify to play on receipt of a token which will be issued through the following methods:</p>
                    <p>
                        Once a customer has cumulatively staked £/€25 a token will be issued. A customer can only hold one token at any one time.
                        Stakes made once a game is in progress will not contribute towards future tokens Customers can qualify for a token through ad hoc promotions from RaceBets
                    </p>
                </span>
            </div>
            <div className={ getDataClasses() }>
                <p className="text-2xl">How do I Play?</p>
                <span>
                    You are given a pot of £/€1,000,000 and have 20 questions to answer. On each question you must either split your pot or put all of your pot on one answer.
                    <p>
                        Once a question has been resulted any money put on the winning answer will move on to the next question.
                    </p>
                    <p>
                        You will then repeat this process until you have answered all 20 questions, any money remaining after the 20th and last question has been resulted is yours to keep.
                    </p>
                </span>
            </div>
            <div className={ getDataClasses() }>
                <p className="text-2xl">What can I win?</p>
                <span>
                    The maximum winnings from any single is £/€1,000,000.
                </span>
            </div>
            <div className={ getDataClasses() }>
                <p className="text-2xl">When will I get my prize?</p>
                <span>
                    Cash will be credited to your account within 48 hours.
                </span>
            </div>
            <div className={ getDataClasses() }>
                <p className="text-2xl">Other game rules</p>
                <span>
                    This is a Sportsbook (i.e. a fixed odds) product.
                    <p>
                        Tickets hold no monetary value and cannot be exchanged for cash.
                    </p>
                    <p>
                        Tickets are only valid for sixty days from the time you answered your first question. Should this time elapse prior to the game being completed in its entirety,
                        you will be able to claim an amount calculated by halving the remaining pot for every question which remains.
                        For example, if you have £/€200 remaining in your pot with 2 questions still to play, you will be entitled to £/€50 (because the remaining £/€200 will be halved to £/€100 for the first unanswered question and then halved again to £/€50 for the 2nd and final unanswered question).
                        Please note that due to you allowing the game to lapse you will not be automatically credited with this amount and instead will need to contact our customer support team to claim.
                    </p>
                    <p>
                        Only one game can be played at any one time.
                    </p>
                    <p>
                        Winnings are returned in cash.
                    </p>
                    <p>
                        Markets are settled in accordance with The RaceBets Sportsbook Rules and Regulations which are available here.
                    </p>
                    <p>
                        In the event of an incorrect result, RaceBets will cancel this result and re-settle the correct result.
                    </p>
                    <p>
                        Should a customer have already progressed past the question which was resulted incorrectly, they will be brought back to the stage of the game where they would have been,
                        should the question have been settled correctly. For example, should a customer with £/€20,000 remaining on his 5th question split £15,000 on yes and £5,000 on no
                        and the result be incorrectly settled as yes and the customer has already moved to question 7 with still £15,000 remaining,
                        when the question has been resettled the customer will move back to question 6 with £5,000 remaining instead.
                    </p>
                    <p>
                        In the event where a question must be voided (e.g. human error in question, big price movement, non-runners),
                        RaceBets will void this question and all customers will have to answer a new question to progress.
                    </p>
                    <p>
                        RaceBets has the right to suspend a question at any time, should this happen all customers who have played the question will be unable to edit their selection.
                        All customers who have not confirmed their selection will be given a new question to answer at a later point.
                    </p>
                </span>
            </div>
            <div className={ getDataClasses() }>
                <p className="text-2xl">General terms and conditions</p>
                <span>
                    RaceBets reserves the right to suspend or disable the ‘The RaceBets Million’ at any time.
                    <p>
                        If you have been sent an email from RaceBets excluding you from promotions,
                        you will not qualify for this promotion even if you meet the wagering requirements.
                    </p>
                    <p>
                        RaceBets reserves the right to amend this promotion at any time without prior notice and without liability to you where circumstances beyond
                        our reasonable control demand it. These changes may occur due to actions or omission taken by a third party which affect this promotion.
                        Any changes to the promotion will be published in these terms and conditions. Any players who have been materially affected by the changes will be notified.
                    </p>
                    <p>
                        If we notice that the promotion is being abused and/or the bonus terms are being breached, we may take the following actions against such abusers:
                    </p>
                    <p>
                        Revoke and/or cancel any free bets and winnings from free bets that we regard may have been redeemed by misuse of the system; and/or
                        Withhold any winnings from entrants; and/or
                        Players found to be abusing bonus and free bet offers may be barred from receiving further bonuses and free bets; and/or
                        Abusing player accounts may be terminated.
                    </p>
                    <p>
                        A list of what may be deemed abuse may include but is not limited to:
                    </p>
                    <p>
                        Using more than one account;
                        Equal, zero or low margin bets;
                        Placing bets which guarantee a profit independently of the betting results as a result of the free bet;
                        Collusion;
                        Use of Bots;
                        Manipulation of software, exploitation of loopholes or other technical forms of abuse or other behaviour which amounts to deliberate cheating;
                        Masking IP address or using a VPN.
                    </p>
                    <p>
                        RaceBets may ask any customer to provide documentation for the purposes of confirming their identity in line with regulation and our
                        internal security procedures and we may also insist on the provision of a mobile phone number. RaceBets will have absolute discretion with
                        regards to the verification of a customer’s identification. Should we decide that the documentation provided is insufficient or unreliable,
                        we reserve the right to block any crediting of bonuses or free bets to that account.
                    </p>
                    <p>
                        All customer offers are limited to one per person, family, household address, email address, IP address, telephone number,
                        same payment account number (e.g. debit or credit card, Neteller etc), and shared computer, e.g. public library or workplace.
                    </p>
                    <p>
                        Standard RaceBets <a className="underline" rel="noreferrer" href="https://www.racebets.com/static/terms" target="_blank">terms and conditions</a> apply.
                    </p>
                </span>
            </div>
        </div>
    );
}

export default TandC;