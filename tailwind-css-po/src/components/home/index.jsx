import React from 'react';
import RaceBets from '../race-bets-section';

const Home = () => {
    return (
        <div className="flex justify-center min-h-full sm:min-h-screen p-8 bg-secondary dark:bg-primary text-primary">
            <RaceBets />
        </div>
    );
}

export default Home;