import navStore from "./nav.store"
import { NavActions } from "./nav.types"

export const updateSelectedIndex = (newIndex) => {
    const action = {
        selectedIndex: newIndex,
        type: NavActions.UPDATE_SELECTED_INDEX
    }

    navStore.dispatch(action);
}

export const getNavCurrentState = () => navStore.getState();