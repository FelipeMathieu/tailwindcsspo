import { createStore } from "redux";
import { NavActions } from "./nav.types";

function navReducer(state = { selectedIndex: 0 }, action) {
    switch(action.type) {
        case NavActions.UPDATE_SELECTED_INDEX:
            return updateSelectedIndex(state, action);
        default:
            return state;
    }
}

function updateSelectedIndex(state, action) {
    state.selectedIndex = action.selectedIndex;
    return state;
}

const navStore = createStore(navReducer);

export default navStore;