import './App.css';
import TestBody from './components/test-body';

function App() {
  return (
    <div className="App">
      <TestBody />
    </div>
  );
}

export default App;
