import React from 'react';

const IFrameComponent = () => {
    return (
        <div style={{
            height: '100vh',
        }}>
            <iframe src="https://race-bets-test.herokuapp.com/"
                title="ReactBetsComponent"
                frameBorder="0"
                height="100%"
                width="1000"
                scrolling="auto" />
        </div>
    );
}

export default IFrameComponent;